#include <ESP8266WiFi.h> // Library for using the NodeMCU 
#include <PubSubClient.h> // Library for using the Network protcall MQTT in this case
#include <RCSwitch.h>
#include "pass.h"
#include <Arduino.h>
#include <IRremoteESP8266.h>
#include <IRsend.h>


/************************* WiFi Access Point *********************************/
const char* ssid = getwifissid(); // Add your SSID(The Name of the Wifi Router you connect to)
const char* password = getwifipass(); // The PassWord (Wireless Key)

/************************* LinuxMqtt Setup *********************************/
const char* mqtt_server = "broker.hivemq.com";
WiFiClient espClient;
PubSubClient client(espClient);

/************************* Remote Setup *********************************/
// Protocol: RC5, Parameters: D=16U F=16U
const uint16_t command_volup[]  = { 889U, 889U, 1778U, 1778U, 1778U, 889U, 889U, 889U, 889U, 889U, 889U, 889U, 889U, 1778U, 1778U, 889U, 889U, 889U, 889U, 889U, 889U, 65535U };

// Protocol: RC5, Parameters: D=16U F=17U
const uint16_t command_voldown[]  = { 889U, 889U, 1778U, 1778U, 1778U, 889U, 889U, 889U, 889U, 889U, 889U, 889U, 889U, 1778U, 1778U, 889U, 889U, 889U, 889U, 1778U, 889U, 65535U };

// Protocol: RC5, Parameters: D=16U F=35U
const uint16_t command_speakera[]  = { 889U, 889U, 1778U, 1778U, 1778U, 889U, 889U, 889U, 889U, 889U, 889U, 1778U, 1778U, 889U, 889U, 889U, 889U, 1778U, 889U, 889U, 889U, 65535U };

// Protocol: RC5, Parameters: D=16U F=39U
const uint16_t command_speakerb[]  = { 889U, 889U, 1778U, 1778U, 1778U, 889U, 889U, 889U, 889U, 889U, 889U, 1778U, 1778U, 889U, 889U, 1778U, 889U, 889U, 889U, 889U, 889U, 65535U };

// Protocol: RC5, Parameters: D=16U F=13U
const uint16_t command_mute[]  = { 889U, 889U, 1778U, 1778U, 1778U, 889U, 889U, 889U, 889U, 889U, 889U, 889U, 889U, 889U, 889U, 1778U, 889U, 889U, 1778U, 1778U, 889U, 65535U };

const uint16_t kIrLed = 12;  // ESP8266 GPIO pin to use. Recommended: 4 (D2).

IRsend irsend(kIrLed);  // Set the GPIO to be used to sending the message.

void happyblink(int amount){
  for(int i = 0;i < amount; i++){
    digitalWrite(LED_BUILTIN,!digitalRead(LED_BUILTIN));
    delay(100);
  }
  digitalWrite(LED_BUILTIN,HIGH);
}

void setup_wifi() {
  pinMode(LED_BUILTIN, OUTPUT);
  delay(100);
  // We start by connecting to a WiFi network
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
    Serial.print(".");
  }
  randomSeed(micros());
  Serial.println("");
  Serial.println("WiFi connected");
  happyblink(10);
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}
void reconnect() {
  // Loop until we're reconnected
  while (!client.connected())
  {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    Serial.print("ClientId:");
    Serial.println(clientId);
    // Attempt to connect
    //if you MQTT broker has clientID,username and password
    //please change following line to    if (client.connect(clientId,userName,passWord))
    if (client.connect(clientId.c_str()))
    {
      Serial.println("connected");
      happyblink(2);
      Serial.println(client.subscribe("esp826601/IRRemoteFA930"));

    }
    else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 6 seconds before retrying
      delay(6000);
    }
  }
} //end reconnect()


void setup() {
  Serial.begin(115200, SERIAL_8N1, SERIAL_TX_ONLY);
  irsend.begin();
  setup_wifi(); // add Method for the Wifi setup
  client.setServer(mqtt_server, 1883); // connects to the MQTT broker
  client.setCallback(callback);
  

}

void callback(char* topic, byte* message, unsigned int length) {
  Serial.print("Message arrived on topic: ");
  happyblink(1);
  Serial.print(topic);
  Serial.print(". Message: ");
  String messageRemote;
  for (int i = 0; i < length; i++) {
    Serial.println((char)message[i]);
    messageRemote += (char)message[i];
  }

  if(String(topic) == "esp826601/IRRemoteFA930") {
    if(messageRemote == "volup") {
        irsend.sendRaw(command_volup, sizeof(command_volup) / sizeof(command_volup[0]), 36);
        Serial.println("Volume Up");
    }
  else if(messageRemote == "voldown") {
        
        irsend.sendRaw(command_voldown, sizeof(command_voldown) / sizeof(command_voldown[0]), 36);
        Serial.println("Volume Down");
  }
  else if(messageRemote == "mute") {
    
        irsend.sendRaw(command_mute, sizeof(command_mute) / sizeof(command_mute[0]), 36);
        Serial.println("Toggle Mute");
  }
  else if(messageRemote == "speakera") {
    
        irsend.sendRaw(command_speakera, sizeof(command_speakera) / sizeof(command_speakera[0]), 36);
        Serial.println("Toggle Speaker A");
        }

  else if(messageRemote == "speakerb") {
        irsend.sendRaw(command_speakerb, sizeof(command_speakerb) / sizeof(command_speakerb[0]), 36);
        Serial.println("Toggle Speaker B");
  }

  else {
    
        Serial.print("Dont know command: ");
        Serial.println(messageRemote);
  }
      
    
  }
}
void loop() {

    if (!client.connected()) {
    reconnect();
  } // MQTT
  client.loop();

}
