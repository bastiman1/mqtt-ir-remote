# MQTT and NODERED BASED IR REMOTE


## working principle

3 Elements are involved

1. NodeMCU with IR Sender
2. MQTT Server
3. NODEred Server

- NODERed supplies interface for sending commands with a specific topic to the MQTTServer
- NodeMCU listens on MQTT Server for commands for a corresponding topic like `ESP_8266_IR_AMP`


